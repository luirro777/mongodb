# Mongo + Mongo Express

Archivo de orquestación para levantar vía docker-compose una instancia de MongoDB, con una interfaz web para administración.

## Uso

`git clone https://gitlab.com/luirro777/mongodb`

`cd mongodb`

`docker-compose up -d`

Con esto se podrá acceder al administrador web desde http://127.0.0.1/8081.

Usuario: webadmin, Pasword: 1234

Pueden modificar esos parámetros desde el archivo .env



